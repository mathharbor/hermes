# Project Hermes - based on the Courseware Project


## Installation

1. Clone the repository
2. Run `bundle install`
3. Run migrations `bundle exec rake db:create db:migrate db:test:prepare`
4. Install some seed data `rake db:seed:development`(Skip this step)
5. Run `rails s
6. Open your browser and navigate to `http://localhost:3000`

You are all set now! Use you local system username (ex.: `ENV['USER']`) or `dev`
and append `@coursewa.re` to get the seeded user email account.
Your password is `secret` (the same for any pre-seeded user). Any new users who register, will be sent confirmation links - in the absence of an email server, these emails will be piped to the terminal log.

Every new course/classroom is hosted on its own subdomain. While this works well in production, we have to figure it out better for dev environments. To sort this out, use lvh.me instead of localhost as a url address. For example, 'http://lvh.me:3000'. So, let's say, we have a class called 'Test', then the subdomain would be 'test.lvh.me'. No need to make any changes to the code or anything, just type lvh.me, instead of localhost, and you're set.

## Usage

Settings can be found
in the `config/initializers/8_courseware.rb` file.

The includes settings for default email, allowed sub-domains, image sizes,
subscription plans and other options.

For a full list of options, check the `coursewareable` project which is the
core of the application.

You can use any database supported by Active Record.
We recommend you PostgreSQL if you plan to use it in production.

The app uses `Delayed::Job` for background processing. If you plan to use it
in production, please take a look on how we suggest deploying it.

## Development

If you write Ruby, we recommend using
[GitHub code style](https://github.com/styleguide/ruby).

We use YARD for documentation. The simple rule is to have
at least one line that describes the module, class or method you wrote.

The UI uses a lot of [Zurb Foundation](http://foundation.zurb.com/)
components. The library we use is still at v3.

### QA

To run some quality checks use the rake task: `bundle exec rake qa`

## Deploying Courseware

Courseware uses `mina` for deployments. Please check the `config/deploy.rb` file
for if you want to set it up.
